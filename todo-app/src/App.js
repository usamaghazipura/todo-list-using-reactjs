import React from 'react';
import {
  ChakraProvider,
  Box,
  Text,
  Input,
  Button,
  Checkbox,
  extendTheme,
  Container,
  Center,
  chakra,
} from '@chakra-ui/react';
import { motion, isValidMotionProp } from 'framer-motion';
const ChakraBox = chakra(motion.div, {
  /**
   * Allow motion props and the children prop to be forwarded.
   * All other chakra props not matching the motion props will still be forwarded.
   */
  shouldForwardProp: (prop) => isValidMotionProp(prop) || prop === 'children',
});

// 2. Add your color mode config
const config = {
  initialColorMode: 'light',
  useSystemColorMode: false,
}

// 3. extend the theme

const toDoItems = [];
class CreateItem extends React.Component {
  handleCreate(e) {
    e.preventDefault();
    
    if (!this.refs.newItemInput.value) {
      alert('Please enter a task name.');
      return;
    } else if (this.props.toDoItems.map(element => element.name).indexOf(this.refs.newItemInput.value) != -1) {
      alert('This task already exists.');
      this.refs.newItemInput.value = '';
      return;
    }
    
    this.props.createItem(this.refs.newItemInput.value);
    this.refs.newItemInput.value = '';
  }
  
  render() {
    return (
      <ChakraBox animate={{ scale: [1, 2, 2, 1, 1], rotate: [0, 0, 270, 270, 0], }} transition={{ duration: 3, ease: "easeInOut", repeatType: "loop", }} display="flex" justifyContent="center" alignItems="center" borderLeftRadius="20">
        <div className="create-new">
        <form onSubmit={this.handleCreate.bind(this)}>
          <Input borderLeftRadius="20" py={5} bg='black' color="gray" htmlSize={20} width='auto' type="text" placeholder="New Task" ref="newItemInput" />
         <button><Button borderLeftRadius="20" borderRightRadius="20" pb={4} bg='black' color="gray">Add</Button></button>
        </form>
      </div>
      </ChakraBox>

    );
  }
}

class ToDoListItem extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      editing: false
    };
  }
  
  renderName() {
    const itemStyle = {
      'text-decoration': this.props.completed ? 'line-through' : 'none',
      cursor: 'pointer'
    };
    
    if(this.state.editing) {
      return (
          <form onSubmit={this.onSaveClick.bind(this)}>
            <input type="text" ref="editInput" defaultValue={this.props.name} />
          </form>
      );
    }
    
    return (
      <span style={itemStyle} onClick={this.props.toggleComplete.bind(this, this.props.name)}>{this.props.name}</span>
    );
  }
  
  renderButtons() {
    if (this.state.editing) {
      return (
        <span className="flex justify-center gap-2">
          <button onClick={this.onSaveClick.bind(this)}>Save</button>
          <button onClick={this.onCancelClick.bind(this)}>Cancel</button>
        </span>
      );
    }
    
    return (
      <span className="flex justify-center gap-2">
        <button onClick={this.onEditClick.bind(this)}>Edit</button>
        <button onClick={this.props.deleteItem.bind(this, this.props.name)}>Delete</button>
      </span>
    );
  }
  
  onEditClick() {
    this.setState({ editing: true });
  }
  
  onCancelClick() {
    this.setState({ editing: false });
  }
  
  onSaveClick(e) {
    e.preventDefault();
    this.props.saveItem(this.props.name, this.refs.editInput.value);
    this.setState({ editing: false });
  }
  
  render() {
    return (
      <motion.div initial="hidden" animate="visible" variants={{
        hidden: {
          scale: .8,
          opacity: 0
        },
        visible: {
          scale: 1,
          opacity: 1,
          transition: {
            delay: 1
          }
        },
      }} gap={20} bg='orange' w="100vh" display="flex" alignItems="center" justifyContent="center">
      <div className="to-do-item">
      <Box gap={20} display="flex" alignItems="center" justifyContent="space-between" as={motion.div} bg='orange' drag='x' dragConstraints={{ left: -100, right: 100 }} whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }} transition='0.5s linear'>
       <Text px={20} py={10} bg='tomato' color="white">
       <motion.div initial="hidden" animate="visible" variants={{
        hidden: {
          scale: .8,
          opacity: 0
        },
        visible: {
          scale: 1,
          opacity: 1,
          transition: {
            delay: 1
          }
        },
      }}
      >
       <span className="name">
        {this.renderName()}
        </span>
        </motion.div>
       </Text>
        <Button bg='black'>
        <span className="actions">
        {this.renderButtons()}
        </span>
        </Button>
        <Checkbox defaultChecked>Checkbox</Checkbox>
        </Box>
      </div>
      </motion.div>
    );
  }
}

class ToDoList extends React.Component {
  renderItems() {
    return this.props.toDoItems.map((item, index) => <ToDoListItem key={index} {...item} {...this.props} />);
  }
  render() {
    return (
      
      <div className="items-list text-center">
        <motion.div initial="hidden" animate="visible" variants={{
        hidden: {
          scale: .8,
          opacity: 0
        },
        visible: {
          scale: 1,
          opacity: 1,
          transition: {
            delay: 1
          }
        },
      }}>
        {this.renderItems()}
        </motion.div>
      </div>

    );
  }
}

class App extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      toDoItems
    };
  }
  
  createItem(item) {
    this.state.toDoItems.unshift({
      name: item,
      completed: false
    });
    this.setState({
      toDoItems: this.state.toDoItems
    });
  }
  
  findItem(item) {
    return this.state.toDoItems.filter((element) => element.name === item)[0];
  }
  
  toggleComplete(item) {
    let selectedItem = this.findItem(item);
    selectedItem.completed = !selectedItem.completed;
    this.setState({ toDoItems: this.state.toDoItems });
  }
  
  saveItem(oldItem, newItem) {
    let selectedItem = this.findItem(oldItem);
    selectedItem.name = newItem;
    this.setState({ toDoItems: this.state.toDoItems });
  }
  
  deleteItem(item) {
    let index = this.state.toDoItems.map(element => element.name).indexOf(item);
    this.state.toDoItems.splice(index, 1);
    this.setState({ toDoItems: this.state.toDoItems });
  }
  
  render() {
    return (
      <Center bg='tomato' h="100vh">
      <div className="to-do-app">
        <div className="header">
        <motion.div initial="hidden" animate="visible" variants={{
  hidden: {
    scale: .8,
    opacity: 0
  },
  visible: {
    scale: 1,
    opacity: 1,
    transition: {
      delay: 1
    }
  },
}} display="flex" alignItems="center" justifyContent="center"><Text as={motion.div} bg='orange.400' display="flex" alignItems="center" justifyContent="center" drag='x' dragConstraints={{ left: -100, right: 100 }} whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }} transition='0.5s linear' fontWeight='bold' textTransform='uppercase' fontSize='40px' letterSpacing='wide' color='teal.600'>
            TODO LIST
        </Text></motion.div>
        </div>
        <CreateItem toDoItems={this.state.toDoItems} createItem={this.createItem.bind(this)} />
        <Text gap={10}><ToDoList toDoItems={this.state.toDoItems} deleteItem={this.deleteItem.bind(this)} saveItem={this.saveItem.bind(this)} toggleComplete={this.toggleComplete.bind(this)} /></Text>
      </div>
      </Center>
    );
  }
}


export default App;